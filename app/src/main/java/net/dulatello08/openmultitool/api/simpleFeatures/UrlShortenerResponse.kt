package net.dulatello08.openmultitool.api.simpleFeatures

import kotlinx.serialization.Serializable

@Serializable
data class UrlShortenerResponse(
    val code: String
)
