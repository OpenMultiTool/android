package net.dulatello08.openmultitool.api

import kotlinx.serialization.Serializable


@Serializable
data class UserRegisterModel(
    val email: String,
    val username: String,
    val password: String
)
