package net.dulatello08.openmultitool.api

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface APIRepository {
    //auth
    @Headers("Content-Type: application/json")
    @POST("api/users")
    suspend fun loginUser(@Body requestBody: RequestBody): Response<ResponseBody>
    @Headers("Content-Type: application/json")
    @PUT("api/users")
    suspend fun registerUser(@Body requestBody: RequestBody): Response<ResponseBody>
    @Headers("Content-Type: application/json")
    @HTTP(method="DELETE", path="api/users", hasBody = true)
    suspend fun removeUser(@Body requestBody: RequestBody): Response<ResponseBody>
    //simpleFeatures
    //link shorten
    @Headers("Content-Type: application/json")
    @POST("https://gotiny.cc/api")
    suspend fun shortenLink(@Body requestBody: RequestBody): Response<ResponseBody>
}