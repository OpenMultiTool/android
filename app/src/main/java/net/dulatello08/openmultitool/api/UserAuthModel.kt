package net.dulatello08.openmultitool.api

import kotlinx.serialization.Serializable


@Serializable
data class UserAuthModel(
    val email: String,
    val password: String,
)