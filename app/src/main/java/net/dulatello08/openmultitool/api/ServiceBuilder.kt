package net.dulatello08.openmultitool.api

import okhttp3.OkHttpClient
import retrofit2.HttpException
import retrofit2.Retrofit

object ServiceBuilder {
    private val client = OkHttpClient.Builder().build()
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://openmultitool.herokuapp.com/") // change this IP for testing by your actual machine IP
        .client(client)
        .build()
    fun<T> buildService(service: Class<T>): T{
        return retrofit.create(service)
    }
}