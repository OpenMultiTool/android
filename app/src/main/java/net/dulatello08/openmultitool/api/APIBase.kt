@file:Suppress("BlockingMethodInNonBlockingContext")

package net.dulatello08.openmultitool.api

import android.content.Context
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.encodeToJsonElement
import net.dulatello08.openmultitool.api.simpleFeatures.UrlShortenerResponse
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import retrofit2.Response


open class APIBase {
    private val sharedPrefFile = "account"
    open suspend fun loginUser(data: UserAuthModel,context: Context, lambda: (Boolean) -> Unit) {
        val retrofit = ServiceBuilder.buildService(APIRepository::class.java)
        val jsonData = Json.encodeToJsonElement(data).toString()
        val requestBody = jsonData.toRequestBody("application/json".toMediaTypeOrNull())
        val shpEditor = context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE).edit()
        Log.e("Data -> ", jsonData)
        CoroutineScope(Dispatchers.IO).launch {
            val response = retrofit.loginUser(requestBody)
            withContext(Dispatchers.Main) {
                Log.e("This -> ", response.code().toString())
                val code = response.code()
                if(code==200){
                    shpEditor.putString("email", data.email)
                        .putString("password", data.password)
                        .apply()
                }
                lambda(code==200)
            }
        }
    }
    open suspend fun registerUser(data: UserRegisterModel, lambda: (Response<ResponseBody>) -> Unit){
        val retrofit = ServiceBuilder.buildService(APIRepository::class.java)
        val jsonData = Json.encodeToJsonElement(data).toString()
        val requestBody = jsonData.toRequestBody("application/json".toMediaTypeOrNull())
        CoroutineScope(Dispatchers.IO).launch {
            val response = retrofit.registerUser(requestBody)
            withContext(Dispatchers.Main) {
                lambda(response)
            }
        }
    }
    open suspend fun deleteUser(data: UserAuthModel, lambda: (Int) -> Unit){
        val retrofit = ServiceBuilder.buildService(APIRepository::class.java)
        val jsonData = Json.encodeToJsonElement(data).toString()
        val requestBody = jsonData.toRequestBody("application/json".toMediaTypeOrNull())
        CoroutineScope(Dispatchers.IO).launch {
            val responseCode = retrofit.removeUser(requestBody).code()
            withContext(Dispatchers.Main){
                lambda(responseCode)
            }
        }
    }

    private val json = Json { ignoreUnknownKeys = true }

    open suspend fun linkShortener(requestBody: String, lambda: (UrlShortenerResponse) -> Unit){
        val retrofit = ServiceBuilder.buildService(APIRepository::class.java)
        val body = requestBody.toRequestBody("application/json".toMediaTypeOrNull())
        CoroutineScope(Dispatchers.IO).launch {
            val response = retrofit.shortenLink(body)
            withContext(Dispatchers.Main){
                val responseBody = response.body()?.string()
                val unwrappedBody = responseBody!!.replaceFirst(Regex("\\["), "").reversed().replaceFirst("]", "").reversed()
                val jsonData = json.decodeFromString<UrlShortenerResponse>(unwrappedBody)
                lambda(jsonData)
            }
        }
    }
}