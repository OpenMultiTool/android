package net.dulatello08.openmultitool.fragments

import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.make
import kotlinx.coroutines.launch
import net.dulatello08.openmultitool.R
import net.dulatello08.openmultitool.api.APIBase
import net.dulatello08.openmultitool.api.UserRegisterModel


class RegisterFragment: Fragment() {
    private fun String.isValidEmail() = !TextUtils.isEmpty(this) && Patterns.EMAIL_ADDRESS.matcher(this).matches()
    private val mApiBase: APIBase = APIBase()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_register, container, false)
        val activity = requireActivity() as AppCompatActivity
        activity.supportActionBar?.hide()
        val email = view.findViewById<EditText>(R.id.emailRegister)
        val username = view.findViewById<EditText>(R.id.usernameRegister)
        val password = view.findViewById<EditText>(R.id.passwordRegister)
        val registerButton = view.findViewById<Button>(R.id.registerButton)
        val loginFragment = LoginFragment()
        registerButton.setOnClickListener {
            if (email.text.isNotEmpty() ||username.text.isNotEmpty()||password.text.isNotEmpty()) {
                if(email.text.toString().isValidEmail()){
                    //Now make http request cuz everything is valid
                    val registerData = UserRegisterModel(email.text.toString(), username.text.toString(), password.text.toString())
                    lifecycleScope.launch{
                        mApiBase.registerUser(registerData){
                            val body = it
                            when (it.code()) {
                                201 -> {
                                    activity.supportActionBar?.show()
                                    parentFragmentManager.commit {
                                        replace(R.id.mainFragmentContainer, loginFragment)
                                        addToBackStack(null)
                                    }
                                    return@registerUser
                                }
                                409 -> {
                                    if(body.equals("same email")) make(view,"The email has already been used", Snackbar.LENGTH_LONG).show()
                                    else make(view,"The username was taken", Snackbar.LENGTH_LONG).show()
                                    return@registerUser
                                }
                                else -> {
                                    make(view, "Failed to connect to server. Try again later", Snackbar.LENGTH_INDEFINITE)
                                        .setAction("RETRY"){
                                            make(view, "Failed to connect to server. Try again later", Snackbar.LENGTH_SHORT).show()
                                        }
                                        .show()
                                }
                            }
                        }
                    }
                }else{
                    make(view, "E-Mail is not valid", Snackbar.LENGTH_LONG).show()
                }
            }else{
                make(view, "Please enter all required fields", Snackbar.LENGTH_LONG).show()
            }
        }
        return view
    }
}