package net.dulatello08.openmultitool.fragments.simpleFeatures

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.LENGTH_LONG
import com.google.android.material.snackbar.Snackbar.make
import kotlinx.coroutines.launch
import net.dulatello08.openmultitool.R
import net.dulatello08.openmultitool.api.APIBase

class ShortenerFragment: SimpleFeaturesFragment(R.layout.fragment_link_shorten) {
    private val shortenButton: Button by lazy {requireView().findViewById(R.id.shortenButton)}
    private val urlInput: EditText by lazy {requireView().findViewById(R.id.shortenLinkInput)}
    private var mInterstitialAd: InterstitialAd? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_link_shorten, container, false)
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(requireActivity(),"ca-app-pub-7509088958653785/3555029250", adRequest, object : InterstitialAdLoadCallback() {
            override fun onAdFailedToLoad(adError: LoadAdError) {
                mInterstitialAd = null
            }

            override fun onAdLoaded(interstitialAd: InterstitialAd) {
                mInterstitialAd = interstitialAd
            }
        })
        mInterstitialAd?.fullScreenContentCallback = object: FullScreenContentCallback() {
            override fun onAdShowedFullScreenContent() {
                mInterstitialAd = null
            }
        }
        val mApiBase = APIBase()
        val clipBoard = requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        shortenButton.setOnClickListener{
            mInterstitialAd?.show(requireActivity()) ?:
            if (urlInput.text.isNotEmpty()) {
                if(Patterns.WEB_URL.matcher(urlInput.text).matches()){
                    //continue
                    val data = """{"input": "${urlInput.text}"}"""
                    lifecycleScope.launch{
                        mApiBase.linkShortener(data) {
                            Log.e("This -> ", it.code)
                            val linkCode = it.code
                            val snack = make(view, "https://gotiny.cc/${it.code}", Snackbar.LENGTH_INDEFINITE)
                                .setAction("COPY"){
                                    val clipData = ClipData.newPlainText("shortUrl","https://gotiny.cc/${linkCode}")
                                    clipBoard.setPrimaryClip(clipData)
                                    urlInput.setText("")
                                }
                            snack.show()
                        }
                    }
                    Log.i("URL Shortener", data)

                }else{
                    make(view, "Not valid URL", LENGTH_LONG).show()
                }
            }else {
                make(view, "Please enter required field!", Snackbar.LENGTH_SHORT).show()
            }
        }
        return view
    }
}