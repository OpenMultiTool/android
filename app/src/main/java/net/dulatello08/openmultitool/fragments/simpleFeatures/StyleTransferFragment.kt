package net.dulatello08.openmultitool.fragments.simpleFeatures

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStorageDirectory
import android.provider.MediaStore
import android.provider.MediaStore.Images
import android.provider.MediaStore.Images.Media.DATA
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.make
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.dulatello08.openmultitool.R
import net.dulatello08.openmultitool.api.ImageUtils
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.gpu.CompatibilityList
import org.tensorflow.lite.gpu.GpuDelegate
import java.io.*
import java.io.File.separator
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel


class StyleTransferFragment: SimpleFeaturesFragment(R.layout.fragment_style_transfer){
    private val compatList = CompatibilityList()
    private var interpreterPredict: Interpreter? = null
    private var interpreterTransform: Interpreter? = null
    private var contentTensor: Bitmap? = null
    private var styleBottleneck: Any? = null
    private var mInterstitialAd: InterstitialAd? = null
    private fun Context.openAppSystemSettings() {
        startActivity(Intent().apply {
            action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            data = Uri.fromParts("package", packageName, null)
        })
    }
    @Throws(IOException::class)
    private fun getInterpreter(
        context: Context,
        modelName: String?,
        modelNameGpu: String?
    ): Interpreter {
        val tfliteOptions = Interpreter.Options()
        if(compatList.isDelegateSupportedOnThisDevice){
            tfliteOptions.addDelegate(GpuDelegate(compatList.bestOptionsForThisDevice))
            return Interpreter(loadModelFile(context, modelNameGpu!!), tfliteOptions)
        }
        tfliteOptions.setNumThreads(4)
        return Interpreter(loadModelFile(context, modelName!!), tfliteOptions)
    }
    @Throws(IOException::class)
    private fun loadModelFile(context: Context, modelFile: String): MappedByteBuffer {
        val fileDescriptor = context.resources.assets.openFd(modelFile)
        val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel = inputStream.channel
        val startOffset = fileDescriptor.startOffset
        val declaredLength = fileDescriptor.declaredLength
        val retFile = fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
        fileDescriptor.close()
        return retFile
    }
    private var contentResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            // There are no request codes
            val data = result.data?.data
            val input = requireContext().contentResolver.openInputStream(data!!)
            Log.e("This ->", data.toString())
            contentTensor = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(input), 384, 384, true)
        }
    }
    private var styleResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            // There are no request codes
            val data = result.data?.data
            val input = requireContext().contentResolver.openInputStream(data!!)
            val styleImage = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(input), 256, 256, true)
            styleBottleneck = predictStyle(styleImage)
        }
    }
    private fun predictStyle(bitmap: Bitmap): Any? {
        val input = ImageUtils.bitmapToByteBuffer(bitmap, 256, 256)
        val inputsForPredict = arrayOf<Any>(input)
        val outputsForPredict = HashMap<Int, Any>()
        val styleBottleneck = Array(1) { Array(1) { Array(1) { FloatArray(100) } } }
        outputsForPredict[0] = styleBottleneck
        interpreterPredict!!.runForMultipleInputsOutputs(inputsForPredict, outputsForPredict)
        Log.e("Done style ->", outputsForPredict[0].toString())
        return  outputsForPredict[0]
    }
    private fun transformImage(content: Bitmap, styleBottleneck: Any?): Bitmap {
        val contentArray = ImageUtils.bitmapToByteBuffer(content, 384, 384)
        //val inputsForStyleTransfer =
        val outputsForStyleTransfer = HashMap<Int, Any>()
        val outputImage = Array(1) { Array(384) { Array(384) { FloatArray(3) } } }
        outputsForStyleTransfer[0] = outputImage
        interpreterTransform!!.runForMultipleInputsOutputs(
            arrayOf(contentArray, styleBottleneck!!),
            outputsForStyleTransfer
        )
        return ImageUtils.convertArrayToBitmap(outputImage, 384, 384)
    }
    private val requestPermission = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        if(!it){
            AlertDialog.Builder(requireContext())
                .setTitle("Permission wasn't granted")
                .setNegativeButton("Cancel"){dialog, _ ->
                    dialog.dismiss()
                }
                .setPositiveButton("Open Settings"){_, _ ->
                    requireContext().openAppSystemSettings()
                }
        }
    }
    /// @param folderName can be your app's name
    private fun String.saveImage(bitmap: Bitmap, context: Context) {
        if (android.os.Build.VERSION.SDK_INT >= 29) {
            val values = contentValues()
            values.put(Images.Media.RELATIVE_PATH, "Pictures/${this}")
            values.put(Images.Media.IS_PENDING, true)
            // RELATIVE_PATH and IS_PENDING are introduced in API 29.

            val uri: Uri? = context.contentResolver.insert(Images.Media.EXTERNAL_CONTENT_URI, values)
            if (uri != null) {
                saveImageToStream(bitmap, context.contentResolver.openOutputStream(uri))
                values.put(Images.Media.IS_PENDING, false)
                context.contentResolver.update(uri, values, null, null)
            }
        } else {
            val directory = File(getExternalStorageDirectory(), separator, )
            // getExternalStorageDirectory is deprecated in API 29

            if (!directory.exists()) {
                directory.mkdirs()
            }
            val fileName = System.currentTimeMillis().toString() + ".png"
            val file = File(directory, fileName)
            saveImageToStream(bitmap, FileOutputStream(file))
            val values = contentValues().apply {
                put(DATA, file.absolutePath)
            }
            // .DATA is deprecated in API 29
            context.contentResolver.insert(Images.Media.EXTERNAL_CONTENT_URI, values)
        }
    }

    private fun contentValues() : ContentValues {
        val values = ContentValues()
        values.put(Images.Media.MIME_TYPE, "image/png")
        values.put(Images.Media.DATE_ADDED, System.currentTimeMillis() / 1000)
        values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis())
        return values
    }

    private fun saveImageToStream(bitmap: Bitmap, outputStream: OutputStream?) {
        if (outputStream != null) {
            try {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
                outputStream.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_style_transfer, container, false)
        val chooseContentBtn = view.findViewById<Button>(R.id.chooseContentImage)
        val chooseStyleBtn = view.findViewById<Button>(R.id.chooseStyleImage)
        val runInferenceBtn = view.findViewById<Button>(R.id.runInference)
        val imageView = view.findViewById<ImageView>(R.id.styledImage)
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(requireActivity(),"ca-app-pub-7509088958653785/7105700956", adRequest, object : InterstitialAdLoadCallback() {
            override fun onAdFailedToLoad(adError: LoadAdError) {
                Log.e("Ads loaded", "false")
                mInterstitialAd = null
            }

            override fun onAdLoaded(interstitialAd: InterstitialAd) {
                Log.e("Ads loaded", "true")
                mInterstitialAd = interstitialAd
            }
        })
        mInterstitialAd?.fullScreenContentCallback = object: FullScreenContentCallback() {
            override fun onAdShowedFullScreenContent() {
                mInterstitialAd = null
            }
        }
        interpreterTransform = if(compatList.isDelegateSupportedOnThisDevice){
            getInterpreter(requireContext(), null,"style_transfer_gpu_384.tflite")
        }else{
            getInterpreter(requireContext(), "style_transfer_quantized_384.tflite", null    )
        }
        interpreterPredict = if(compatList.isDelegateSupportedOnThisDevice) {
            getInterpreter(requireContext(),null ,"style_predict_gpu_256.tflite")
        }else{
            getInterpreter(requireContext(), "style_predict_quantized_256.tflite", null)
        }
        runInferenceBtn.setOnClickListener {
             if (mInterstitialAd!=null) mInterstitialAd?.show(requireActivity())
             val styledImage = transformImage(contentTensor!!, styleBottleneck)
             imageView.setImageBitmap(styledImage)
             make(view, "Image generated", Snackbar.LENGTH_INDEFINITE)
                    .setAction("SAVE") {
                           "OpenMultiTool".saveImage(styledImage, requireContext())
                    }
                    .show()
        }

        chooseContentBtn.setOnClickListener {
            requestPermission.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            intent.putExtra("content", true)
            contentResultLauncher.launch(intent)
        }
        chooseStyleBtn.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            intent.putExtra("content", false)
            styleResultLauncher.launch(intent)
        }
        return view
    }
}