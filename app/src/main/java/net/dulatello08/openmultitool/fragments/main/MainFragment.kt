package net.dulatello08.openmultitool.fragments.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import net.dulatello08.openmultitool.R
import net.dulatello08.openmultitool.fragments.simpleFeatures.ShortenerFragment
import net.dulatello08.openmultitool.fragments.simpleFeatures.StyleTransferFragment


class MainFragment: Fragment(R.layout.fragment_main) {
    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedinstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_main, container, false)
        val shortenLink = view.findViewById<ConstraintLayout>(R.id.shortenURL)
        val shortenFragment = ShortenerFragment()
        shortenLink.setOnClickListener {
            parentFragmentManager.commit {
                setCustomAnimations(R.anim.slide_in, R.anim.slide_out)
                replace(R.id.mainFragmentContainer, shortenFragment)
                addToBackStack(null)
            }
        }
        val styleTransfer = view.findViewById<ConstraintLayout>(R.id.mergeImages)
        val styleTransferFragment = StyleTransferFragment()
        styleTransfer.setOnClickListener{
            parentFragmentManager.commit {
                setCustomAnimations(R.anim.slide_in, R.anim.slide_out)
                replace(R.id.mainFragmentContainer, styleTransferFragment)
                addToBackStack(null)
            }
        }
        return view
    }
}