package net.dulatello08.openmultitool.fragments.simpleFeatures

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import net.dulatello08.openmultitool.R
import net.dulatello08.openmultitool.api.APIBase

open class SimpleFeaturesFragment(private var layoutFile: Int) : Fragment()  {
    val mApiBase = APIBase()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View? = inflater.inflate(layoutFile, container, false)
        return view
    }
}