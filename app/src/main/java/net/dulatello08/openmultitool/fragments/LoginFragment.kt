package net.dulatello08.openmultitool.fragments

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.make
import kotlinx.coroutines.launch
import net.dulatello08.openmultitool.R
import net.dulatello08.openmultitool.api.APIBase
import net.dulatello08.openmultitool.api.UserAuthModel
import net.dulatello08.openmultitool.fragments.main.MainFragment


class LoginFragment: Fragment(){
    private fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }
    private val mApiBase = APIBase()
    @Suppress("SpellCheckingInspection")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        val activity = requireActivity() as AppCompatActivity
        activity.supportActionBar?.hide()
        val sharedPreferences = requireContext().getSharedPreferences("account", Context.MODE_PRIVATE)
        val data = UserAuthModel(sharedPreferences.getString("email", "NOVAL")!!, sharedPreferences.getString("password", "NOVAL")!!)
        val mainFragment = MainFragment()
        lifecycleScope.launch{
            if(data.email!="NOVAL" || data.password!="NOVAL"){
                mApiBase.loginUser(data, requireContext()){
                    if(it){
                        activity.supportActionBar?.show()
                        parentFragmentManager.commit {
                            replace(R.id.mainFragmentContainer, mainFragment)
                            addToBackStack(null)
                        }
                    }
                }
            }
        }
        val email = view.findViewById<EditText>(R.id.emailInput)
        val password = view.findViewById<EditText>(R.id.passwordInput)
        val loginButton = view.findViewById<Button>(R.id.loginButton)
        val registerButton = view.findViewById<Button>(R.id.register)
        registerButton.setOnClickListener{
            parentFragmentManager.commit {
                setCustomAnimations(R.anim.slide_in, R.anim.slide_out)
                replace(R.id.mainFragmentContainer, RegisterFragment())
                addToBackStack(null)
            }
        }
        loginButton.setOnClickListener {
            if((email.text.toString()=="") || (password.text.toString()=="")){
                make(view, "Please fill required fields", Snackbar.LENGTH_LONG).show()
            }else if(email.text.toString().isEmailValid()){
                val authData = UserAuthModel(email.text.toString(), password.text.toString())
                lifecycleScope.launch{
                    mApiBase.loginUser(authData, requireContext()){
                        if(it){
                            activity.supportActionBar?.show()
                            parentFragmentManager.commit {
                                setCustomAnimations(R.anim.slide_in, R.anim.slide_out, R.anim.slide_in, R.anim.slide_out)
                                replace(R.id.mainFragmentContainer, mainFragment)
                                addToBackStack(null)
                            }
                            return@loginUser
                        }else{
                            make(view,"Wrong email/password", Snackbar.LENGTH_LONG).show()
                            return@loginUser
                        }
                    }
                }
            }else{
                make(view, "E-Mail not valid!", Snackbar.LENGTH_LONG).show()
            }
        }
        return view
    }

}