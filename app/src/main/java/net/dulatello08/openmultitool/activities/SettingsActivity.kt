package net.dulatello08.openmultitool.activities

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.media.MediaCodec.MetricsConstants.MODE
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.OnUserEarnedRewardListener
import com.google.android.gms.ads.rewarded.RewardItem
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.make
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.dulatello08.openmultitool.R
import net.dulatello08.openmultitool.api.APIBase
import net.dulatello08.openmultitool.api.UserAuthModel

class SettingsActivity : AppCompatActivity() {
    private val deleteButton: ConstraintLayout by lazy { findViewById(R.id.deleteAccountButton) }
    private val view: ConstraintLayout by lazy { findViewById(R.id.settingsLayout)}
    private val developerButton: ConstraintLayout by lazy { findViewById(R.id.enterDeveloperMode)}
    private val mApiBase = APIBase()
    private var mRewardedAd: RewardedAd? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val adRequest = AdRequest.Builder().build()
        RewardedAd.load(this,"ca-app-pub-3940256099942544/5224354917", adRequest, object : RewardedAdLoadCallback() {
            override fun onAdFailedToLoad(adError: LoadAdError) {
                Log.e("Ads loaded", "false")
                mRewardedAd = null
            }

            override fun onAdLoaded(rewardedAd: RewardedAd) {
                Log.e("Ads loaded", "true")
                mRewardedAd = rewardedAd
            }
        })
        mRewardedAd?.fullScreenContentCallback = object: FullScreenContentCallback() {
            override fun onAdDismissedFullScreenContent() {
                mRewardedAd = null
            }
        }
        developerButton.setOnClickListener {
            val devModeEnabled: Boolean = sharedPref.getBoolean("devMode", false)
            if(devModeEnabled){
                mRewardedAd?.show(this) { TODO("DB does not currently support") }
                sharedPref.edit().putBoolean("devMode", true).apply()
            }else {
                make(view, "Developer mode has already been enabled!", Snackbar.LENGTH_INDEFINITE)
                    .setAction("GO BACK"){
                        startActivity(Intent(this@SettingsActivity, MainActivity::class.java))
                    }
                    .show()
            }
        }
        deleteButton.setOnClickListener {
            val email = applicationContext.getSharedPreferences("account", Context.MODE_PRIVATE).getString("email", "ERRNO")!!
            yesNoDialogCreate{
                askPasswordDialog {password, dialog ->
                    lifecycleScope.launch{
                        withContext(Dispatchers.IO){
                            mApiBase.deleteUser(UserAuthModel(email, password)){
                                if(it==202){
                                    make(this@SettingsActivity, view, "Successfully deleted your account", Snackbar.LENGTH_SHORT).show()
                                    deleteLoggedInUser(this@SettingsActivity)
                                    startActivity(Intent(this@SettingsActivity, MainActivity::class.java))
                                }else{
                                    dialog.cancel()
                                    make(this@SettingsActivity, view, "Wrong password or user does not exist!", Snackbar.LENGTH_SHORT).show()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun yesNoDialogCreate(lambda: () -> Unit) {
        AlertDialog.Builder(this)
            .setTitle("Are you sure you want to delete?")
            .setPositiveButton("Yes") {dialog,_->
                lambda()
                dialog.dismiss()
            }.setNegativeButton("No") { dial, _ ->
                dial.cancel()
            }.create().show()
    }
    private fun askPasswordDialog(lambda: (String, DialogInterface) -> Unit) {
        val builder = AlertDialog.Builder(this)
        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        builder.setView(input)
        builder
           .setTitle("Enter your password")
           .setPositiveButton("OK"){dialog,_->
               lambda(input.text.toString(), dialog)
               dialog.dismiss()
            }
            .setNegativeButton("Cancel"){ dialog,_ ->
               dialog.cancel()
            }
            .create()
            .show()
    }
    private fun deleteLoggedInUser(context: Context){
        context.getSharedPreferences("account", Context.MODE_PRIVATE).edit().remove("loggedIn").apply()
    }
}